<?php

namespace App\Http\Controllers;

use App\Http\Requests\ExchangeRateRequest;
use App\Services\ExchangeRateService;
use Illuminate\Http\Request;

class ExchangeRateController extends Controller
{
    //
    public function index(ExchangeRateRequest $request, ExchangeRateService $svc)
    {
        $validated = $request->validated();

        $result =  $svc->convert(
            $validated['original_currency'],
            $validated['target_currency'],
            $validated['price']
        );
        return response()->json([
            'price'=>$result
        ]);
    }
}
