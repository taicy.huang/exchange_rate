<?php
namespace App\Services;

use Illuminate\Support\Facades\Config;

class ExchangeRateService
{
    public function __construct()
    {
        $this->rate_list = Config::get('exchange_rate.currencies');
    }
    public function convert($original_currency, $target_currency, $price)
    {
        $rate = number_format($this->rate_list[$original_currency][$target_currency], 6);

        return number_format($price, 6)*$rate;
    }
}
