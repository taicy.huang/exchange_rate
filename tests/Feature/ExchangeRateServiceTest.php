<?php
namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ExchangeRateServiceTest extends TestCase
{
    public function test_exchange_rate_happy_path()
    {
        $response = $this->get('/api/exchange_rate?original_currency=USD&target_currency=TWD&price=100');

        $response->assertStatus(200);
        $response->assertJson([
            "price" => 3044.4
        ]);
    }

    public function test_exchange_rate_without_from()
    {
        $response = $this->get('/api/exchange_rate?target_currency=TWD&price=100');

        $response->assertStatus(400);
    }

    public function test_exchange_rate_without_to()
    {
        $response = $this->get('/api/exchange_rate?original_currency=USD&price=100');

        $response->assertStatus(400);
    }

    public function test_exchange_rate_without_price()
    {
        $response = $this->get('/api/exchange_rate?original_currency=USD&target_currency=TWD');

        $response->assertStatus(400);
    }

    public function test_exchange_rate_with_wrong_from()
    {
        $response = $this->get('/api/exchange_rate?original_currency=TEST&target_currency=TWD&price=100');

        $response->assertStatus(400);
    }

    public function test_exchange_rate_with_wrong_to()
    {
        $response = $this->get('/api/exchange_rate?original_currency=USD&target_currency=TEST&price=100');

        $response->assertStatus(400);
    }

    public function test_exchange_rate_with_wrong_price()
    {
        $response = $this->get('/api/exchange_rate?original_currency=USD&target_currency=TWD&price=TEST');

        $response->assertStatus(400);
    }
}
