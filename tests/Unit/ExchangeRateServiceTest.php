<?php

namespace Tests\Unit;

use PHPUnit\Framework\TestCase;
use Illuminate\Support\Facades\Config;

class ExchangeRateServiceTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function test_convert_exchange_rate_happy_path()
    {
        Config::shouldReceive('get')
        ->once()
        ->andReturn([
            "JPY"=>
            [
                "TWD"=>2.222,
            ]
        ]);
        $service = new \App\Services\ExchangeRateService();

        $result = $service->convert(
            'JPY',  //original_currency
            'TWD',  // target_currency
            200     // price
        );
        $this->assertSame(444.4, $result);
    }
}
